Hooks.once("ready", async function () {
  const log = function (message) {
    console.log("%cMoltenModule |", "color: orange", message);
  };

  let fiveMinute;
  let oneMinute;

  if (window.location.hostname.split(".")[1] != "moltenhosting") {
    return;
  }

  const stackName = window.location.hostname.split(".")[0];

  let moltenKey;
  if (typeof game.world.data === "undefined") {
    moltenKey = game.world.moltenKey;
  } else {
    moltenKey = game.world.data.flags.moltenKey;
  }

  log(
    `Detected MoltenHosting server ${stackName}.  Thanks for being a customer!`
  );

  if (moltenKey === undefined) {
    console.error(
      "MoltenModule | No authentication token present, aborting connection."
    );
    return;
  }

  const socket = new WebSocket(
    `wss://socket.moltenhosting.com/Prod?token=${moltenKey}&stackName=${stackName}`
  );

  const ding = () => {
    const dingPath = "modules/molten-module/static/ding.mp3";
    AudioHelper.play(
      { src: dingPath, volume: 0.8, autoplay: true, loop: false },
      true
    );
  };

  const register = () => {
    const data = {
      action: "register",
      data: stackName,
      token: moltenKey,
    };
    socket.send(JSON.stringify(data));
  };

  socket.addEventListener("open", function (event) {
    log("Registering listener with socket.moltenhosting.com");
    register();

    setInterval(() => {
      register();
      console.debug("MoltenModule | Socket heartbeat.");
    }, 300000);
  });

  socket.addEventListener("message", function (event) {
    const payload = JSON.parse(event.data);

    if (payload.state === "OK") {
      ui.notifications.info(
        "[MOLTEN HOSTING] Your server's shutdown timer has been reset."
      );
      clearTimeout(fiveMinute);
      clearTimeout(oneMinute);
    } else if (payload.state === "ALARM") {
      ui.notifications.warn(
        "[MOLTEN HOSTING] Server is shutting down in 10 minutes.  Refresh your browser to keep your server awake."
      );
      ding();

      // 5 Minute Warning
      fiveMinute = setTimeout(() => {
        ui.notifications.warn(
          "[MOLTEN HOSTING] Server is shutting down in 5 minutes.  Refresh your browser to keep your server awake."
        );
        ding();
      }, 300000);

      // 1 Minute Warning
      oneMinute = setTimeout(() => {
        ui.notifications.warn(
          "[MOLTEN HOSTING] Server is shutting down in 1 minute.  Refresh your browser to keep your server awake."
        );
        ding();
      }, 540000);
    }
  });

  socket.addEventListener("error", function (event) {
    console.error("MoltenModule | Error communicating with socket.");
    console.error(event);
  });

  socket.addEventListener("close", function (event) {
    console.error("MoltenModule | Socket with MoltenHosting closed.");
    console.error(event);

    ui.notifications.warn(
      "[MOLTEN HOSTING] Lost connection with socket server.  Refresh your browser to reconnect."
    );
  });
});
