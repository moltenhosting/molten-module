# molten-module

A collection of quality of life improvements for hosting games on MoltenHosting.com.

# Installation

_Note: This module is available on the FVTT module registry._ [Molten Module](https://foundryvtt.com/packages/molten-module).

The `molten-module` is self-configuring! Once installed and enabled within your world it will notify you 10, 5, and 1 minute(s) before your server goes to sleep. Usually, simply refreshing your browser will be enough to keep it awake.

![warning notification](https://molten-cdn-storage.s3.amazonaws.com/warning.png "Warning Notification")

# Support

For support and issues please join our [Discord](https://discord.gg/pzYXEQ3).

# Licenses

`static/ding.mp3` licensed under [CC BY 4.0](https://www.freesoundslibrary.com/single-ding-sound-effect/)
