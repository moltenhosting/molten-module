#!/bin/bash
cat moduleTemplate.json | jq --arg VERSION "$1" '.version |= $VERSION | .manifest |= "https://molten-cdn-storage.s3.amazonaws.com/molten-module/" + $VERSION + "/module.json" | .download |= "https://molten-cdn-storage.s3.amazonaws.com/molten-module/" + $VERSION + "/module.zip"' > module.json
zip -r module.zip . -x '.DS_Store' -x 'deploy.sh' -x '*.zip' -x '.git/*' -x '.gitignore' -x 'README.md'
cat module.json

aws s3 cp --exclude static/ding.mp3 module.json "s3://molten-cdn-storage/molten-module/${1}/module.json"
aws s3 cp --exclude static/ding.mp3 module.zip "s3://molten-cdn-storage/molten-module/${1}/module.zip"
aws s3 cp --exclude static/ding.mp3 module.json s3://molten-cdn-storage/molten-module/latest/module.json
aws s3 cp --exclude static/ding.mp3 module.zip s3://molten-cdn-storage/molten-module/latest/module.zip

rm module.json
rm module.zip

git tag $1
git push origin --tags